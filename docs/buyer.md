Buyer HOW-TO
============

PREV: [Running the SubSpace Market daemon](https://gitlab.com/subspaced/subspaced/blob/master/docs/running.md)

How to Became a Buyer
---------------------

Easy, you just pick a unique user name when you first run subspaced. All your keys will be generated for you, and saved
in '~/.subspaced/user'. After that, you can log in and browse the products, place an order. You can message the seller
and you can ask about the product. Your communication will be end-to-end encrypted, so that no-one else can read them
and there's no need to struggle with PGP (or GPG) manually. Remember, the page you're typing into is not served by some
distant remote server, but by a daemon running *on your local machine*!

1. Placing an Order
-------------------

Once you have found something interesting, you press the blue button with the "cart" icon on the product's profile page,
and you'll be redirected to the New Order page. Don't reload that page, it won't work (for your safety, and to avoid repeated
orders by mistake, it contains a hidden CSRF token).

On that page, you can select the desired payment method. If there's any vendor offering an Escrow Service on the market, you can
optionally select one of them too.

If the seller has checked that their product needs to be shipped, then you'll be asked for a shipping address. Never use your
real address though, pick a safe drop place and use that address. For your safety, previous shipping locations ARE NOT STORED,
so you have to enter the address each time. This way even if somebody steals your computer and cracks the several layers of
military grade encryptions, they won't be able to connect you with any real life locations.

Double check everything, and when every details check out, press the "Confirm Order" button.

2. Waiting for Approval
-----------------------

It is possible that the seller is off-line, or temporarily run out of stock, or distrust the escrow provider of your choosing,
suspects that your drop place has been compromised, or simply just hate your face. So their acknowledge means they have received
your order and they are going to fullfil it.

If you have choosen an escrow, then all the payment details will be provided by them, the order will be handled outside
of the market. Then you'll only see a "Package Received" and a "Dispute" link to finalize the order, but that's all.
Go to step 7.

3. Creating a Multisig Wallet
-----------------------------

If the deal is on, the seller will send you their public key (AAAPUB). You also have to get the public key for your wallet
(BBBPUB). For your safety, we recommend to create a new wallet for each transaction, but that's not mandatory. If you have set
up [bitcoind](https://gitlab.com/subspaced/subspaced/blob/master/docs/bitcoind.md), this step is done automatically. If you're
using Bitcoin QT without RPC, then select "Help" / "Debug window" / "Console" tab, and type the commands there (there'll be a big
red warning about do not enter commands which you don't fully understand, TAKE THAT SERIOUSLY!). Otherwise type these commands in
the command line after your wallet application's name (for example `bitcoin-cli`, others should work too). If you have choosen
direct payment in the previous step, then the seller will send you a wallet address (CCC), so you don't have to create a wallet,
go to step 4.

```sh
getnewaddress
BBB
validateaddress BBB
...
"pubkey": "BBBPUB"
...
addmultisigaddress 2 '["AAAPUB","BBBPUB"]'
CCC
```

The first command, `getnewaddress` creates a new wallet address for you (BBB). The second command, `validateaddress` prints a
lot of details about the wallet, among many things the public key (BBBPUB). The third command, `addmultisigaddress` creates
a new multisig wallet address (CCC) with both the seller's (AAAPUB) and your (BBBPUB) public keys.

4. Paying a Deposit
-------------------

Now that you have a wallet (CCC) to pay to, send the order's worth (0.1 BTC in this example) there. For your convenience, a
QR Code will be displayed too.

```sh
sendtoaddress CCC 0.1
DDD
```

This `sendtoaddress` command transfers the coins from your default wallet to the deposit (or you can use your favourite GUI
application for that. Just remember you have to get the transaction id).

As a proof of payment, you must copy the transaction id (DDD) into the form on the market.

5. Shipment
-----------

Once the seller has confirmed the deposit, they will send the goods IRL, a funds release request (FFF), and optionally a
delivery tracking id and/or url to you. You can use that tracking url to follow your package along the way. If you have
purchased digital goods, then the tracing url will be your download url.

6. Package Received
-------------------

When the package has arrived, you are required to sign their transaction (FFF) to release the funds from the multisig wallet.
Some seller may ask you to FE (Finalize Early), which means to sign their payout transaction before you've recevied anything.
NEVER DO THAT. I bet in real life you never sign a package delivered paper early at the post office either. If you have set up
bitcoind, this step is automatic, you only have to press the "Package Received" button.

```sh
signrawtransaction FFF
GGG
sendrawtransaction GGG
```

The first command, `signrawtransaction` signs their payout request with your digital signature (private key of BBB to be precise).
The second command, `sendrawtransaction` announces the payout transaction to the bitcoin network, thus allowing the seller to
receive their money.

7. Finalizing
-------------

You will be prompted to rate the seller on your satisfaction with the service and the product you purchased. It is very
important to be honest, and rate the seller as correctly as you can. Don't rate down the seller just because you received
nothing! It's possible that dispite their best efforts, the package was lost. Remember, they can't receive any money until
you say so, therefore it is in their best interest
[not to scam you](https://gitlab.com/subspaced/subspaced/blob/master/docs/disputes.md).

Also don't forget the seller will rate you as a buyer too. If you forget to release the funds from the deposit, you'll
be downvoted, and you won't be able to buy again!

Listing Orders
--------------

In the navigation bar, there's an "Orders" link. Use that to find your pending orders. You can search for almost anything,
product name, description, order date (even in ISO date form), and even for the escrow service or the escrow provider's name.
It worth mentioning that everything is stored using strong encryption, so delete closed orders to considerably speed up page
load time. For that reason and also for your safety, closed orders will be automatically deleted after a week.

NEXT: [Seller how-to](https://gitlab.com/subspaced/subspaced/blob/master/docs/seller.md)

