HOW-TO Set up a Product Profile
===============================

PREV: [Seller how-to](https://gitlab.com/subspaced/subspaced/blob/master/docs/seller.md)

Basics
------

First of all, click on the "Sell" link on the top menu bar. This page shows your products you want to sell. At first
it will be empty. Click on the "Add New Product" link.

You'll have to fill in some fields.

## Product's Name and Description

Please fill the name and description in as many language as you can. English is mandatory, others are optional. This may
seem that we think English is superior in any way, but we think not. The thruth is, English is the language of the web,
and it's a fair assumption that a user visiting the DW can - even if just a little bit - understand it. Also it's a very
simple language, very easy to learn as compared to other languages.

## Product Picture

Next you'll have to give a picture for your product. This must be a local JPEG file. You should be able to simply
drag'n'drop a jpeg into the input field. Please note there's no image upload. Subspaced is running LOCALLY on your
machine, and it requires LOCAL file paths.

Pictures will be sanitized, all privacy-related information removed from them (like where and when the picture was
taken), and a thumbnail and a resized large picture will be generated out of them.

## Product Types

Next, you have to specify your product's category and type. Your product won't be listed on
[browse only](https://gitlab.com/subspaced/subspaced/blob/master/docs/browseonly.md) nodes if you don't check the "shown on
browse only nodes" box, meaning only registered and logged in users can see it.

### Item

Simplest, you offer one unique item for a fixed price. When the first order is done, the item is automatically removed
from the market. Ideal for manufactured and handcrafted items, which are one of a kind.

### Product

The most common type. You offer a product which you produce for a fixed price, and the buyers can order it several times.
You can specify versions on your product. Ideal if you mass-produce more of the same item.

### Crowdfunding

This is another simple type. You have to specify a wallet to pay donations to directly, and a target amount. The market
keeps track of the funds raised, and closes the item when the target met or the deadline passes.

### Auction

You have to specify a time and a price limit, and a starting price. The buyers are offering more and more coins, until
either the deadline passes or the target amount met. Then the winner places a normal order as they would with a unique
item sale. After that the item is removed from the market. Ideal if you have a one of a kind item for which you don't
know the price.

### Escrow Service

These are special products, which are not listed on the products page. Instead they are offered during the order process
as a payment option. If you want to sell an Escrow Service, buy the ultimate SubSpace Market Guide to learn how to set up
your node to provide one (not difficult, but needs a script which receives arguments).

## Shipping Locations

If you're setting up a product which needs to be shippend (in opposite to a digital goods), then check that box.
You can speficy where you're willing to ship the purchased item. Because buyers have to give you the exact
shipping address, you can discuss that with them, therefore this is not mandatory. But it helps with the search filters.
Be careful, it's very bad OPSEC if you specify only one country and you write a city name too in the description.

Product Versions and Price
---------------------------

If you have choosen Product Sale, you can specify more versions of the product if you like. One is mandatory. It doesn't
matter what feature you choose, it can be colour or size or whatsoever. Each version will have it's own price. When you
have only one price, then and only then you can omit the version name.

Translating version names could be problematic, because most users use universal, international units (like 1 l, 10 oz,
25 g) or specific brand or model names (like Cherry, Light, No Sugar of Coke for example). If you want to have more
translations, we suggest to separate words by a slash, like: "Black / Schwarz / Negro / Noir / Черный / 黑" etc.

For convenience, the largest amount for a price is 1000000 BTC, and the smallest amount is limited to 0.001 BTC, or 100000
Satoshi. This is also reasonably small (worths a few USD as of writing), and is bigger than the minimal miner's fee by a
magnitude.

Selling Digital Goods
---------------------

SubSpace Market also helps with distributing files. If you create the directory '~/.subspaced/download', you can put your
files there. Then you need to generate download URLs for them with

```
$ subspaced --url filename licensekey
```

If you only specify the filename, then a unique license key will be generated for you. This command outputs a download URL,
and anybody who knows the license key can download the specified file from your node for a limited *three days* time:

```
https://(your hidden service name):8080/download/(license key)
```

You may use this URL in the "Download URL" field on the order form (see
[seller's guide](https://gitlab.com/subspaced/subspaced/blob/master/docs/seller.md) section "4. Payment Request and Shipment").

Using '--url' option is not mandatory. License keys are stored in '~/.subspaced/download/.index', which is a normal, unencrypted
sqlite3 database file. You can also add records to it from a script or with the `sqlite3` command line utility if you want. With
that you can set any expiration time.

NEXT: [Remote Access how-to](https://gitlab.com/subspaced/subspaced/blob/master/docs/remote.md)

