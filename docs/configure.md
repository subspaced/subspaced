Configuring subspaced
=====================

PREV: [Download or compile SubSpace Market daemon](https://gitlab.com/subspaced/subspaced/blob/master/docs/compile.md)

NOTE: you only have to configure it once.

Files
-----

If you'd like to have a system wide configuration, create '/var/lib/subspaced' and give write permission to it before
you run subspaced. Otherwise the market's files will be stored in your home directory, under '~/.subspaced/'.

If you're using Tails OS, then you must set up an
[encrypted presistent storage](https://tails.boum.org/doc/first_steps/presistence/index.en.html) first, and create the
directory '/live/presistence/TailsData_unlocked/subspaced' so that SubSpace Market won't get amnesia every time you reboot.
That would kinda suck as it would have to generate your public/private keys every time, therefore you wouldn't be able to
communicate at all.

Configuration Assistant
-----------------------

When you run subspaced for the first time, it will start in configuration mode to create your config files and keys. You can
invoke this mode later with the argument '--configure' to reconfigure (or you can manually edit '~/.subspaced/market.conf',
see list of all available options below).

This mode requires a terminal and will show you a very simple menu system to guide you through the configuration process.
You can cancel the process any time by pressing <kbd>Ctrl</kbd>+<kbd>C</kbd>. Most of the time it's enough to just hit
<kdb>Enter</kbd> to the questions. This mode will also bootstrap your node on the P2P network and will register a user
account for you on the market.

### Node Names

When you are asked for your 'Tor node name', paste the node name ending in '.onion', acquired in
[this step](https://gitlab.com/subspaced/subspaced/blob/master/docs/anonymizer.md).
Likewise, when asked for 'I2P node name', paste the Server Tunnel's name that ends in '.b32.i2p'. If you omit one of
these names, then the market won't connect to peers accessible on that anonymizer network. You can't omit both, you
have to use at least one node name.

### Peer Nodes

In the step before the last, you'll be asked for a bootstrap node name. You must use a node which is on the same network
as your node, so if you have given only 'Tor node name', then you must supply a node which name ends in '.onion'.
Likewise, if you have set up I2P only, then you have to use a node name ending in '.b32.i2p'. For active SubSpace Market
nodes, you should check DN forums, any node will do as a bootstrap node. Once the initial peer list is downloaded, you don't
have to worry about peers any more, as the list is automatically kept up-to-date. If you have both Tor and I2P connections,
it doesn't matter which one you use.

Reconnect to the Network
------------------------

If it happens that you haven't been used the SubSpace Market for a long time, and your peers database contains old,
inactive nodes only, then subspaced won't be able to connect. To overcome this, delete the peers database, and
reconfigure the P2P network by providing an active, accessible bootstrap node:

```sh
$ rm ~/.subspaced/peers
$ subspaced --configure
```

Configuration Options
---------------------

| Key        | Type   | Value |
| ---------- | ------ | ----- |
| port       | number | listening port (default 8080) |
| token      | string | enable [remote access](https://gitlab.com/subspaced/subspaced/blob/master/docs/remote.md) with token |
| browseonly | 0/1    | enable [browse only mode](https://gitlab.com/subspaced/subspaced/blob/master/docs/browseonly.md) (default 0) |
| localonly  | 0/1    | enable [local market mode](https://gitlab.com/subspaced/subspaced/blob/master/docs/localonly.md) (default 0) |
| user       | string | drop privileges and run as the given UNIX user (defaults to keep current user's rights) |
| torport    | number | tor proxy port (default 9050) |
| tornode    | string | tor node name (ends in .onion) |
| i2pport    | number | I2P proxy port (default 4444) |
| i2pnode    | string | I2P node name (ends in .b32.i2p) |
| btcport    | number | Bitcoin Core JSON RPC API port (default 8332) |
| btcpass    | string | Bitcoin Core JSON RPC API authentication, 'user:pass' |
| lang       | string | force translation. If exists, takes precedence over '-l' command line option |
| verbose    | number | force verbosity level. If exists, takes precedence over '-v' command line option |
| sessionttl | number | user session timeout in seconds (default 1800 sec, half an hour) |
| refresh    | number | status refresh frequency (default 10 seconds) |
| darktheme  | 0/1    | use a dark theme on the web interface (default 0) |

Example Configuration File
--------------------------

~/.subspaced/market.conf:

```
# listening port
port=8080

# Tor network connection
torport=9050
tornode=hbaljkqe6dlfl7cedulsc9fupr3c274btvnqvowlligf3j6sqjiwafgs.onion

# I2P network connection
i2pport=4444
i2pnode=dhsdkjhsdafklj4efkljdsflkj4ro9usfkljasdf98098asdfkja.b32.i2p

# Bitcoind RPC API connection
btport=8332
btpass=SomeReallyLongGeneratedPasswordEvenYouCantRemember

# Web interface
sessionttl=1800 #sec
refresh=10 #sec
darktheme=1
```

NEXT: [Running SubSpace Market daemon](https://gitlab.com/subspaced/subspaced/blob/master/docs/running.md)

