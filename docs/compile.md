Compilation
===========

PREV: [Configure bitcoind](https://gitlab.com/subspaced/subspaced/blob/master/docs/bitcoind.md)

Download Pre-compiled Executable
--------------------------------

SubSpace Market is distributed as a single executable binary, which can be downloaded from
[here](https://gitlab.com/subspaced/subspaced/tree/master/bin). No installation required. Always check the integrity of the
downloaded file. Alternatively you can clone the [source](https://gitlab.com/subspaced/subspaced/tree/master/src) from the git
repo and compile it yourself on your machine.

Compilation
-----------

We wanted to use ANSI C (ISO C89) but unfortunatelly neither OpenSSL nor SQLite3 headers support that, so we came up with
ISO C99. We've elminiated all the run-time dependencies, even sacrificing little bit of performance and modularity for that
goal (no phtread or dlopen for example), everything is statically linked except libc. This should ease porting to other systems,
and should allow the binary to run correctly on wide range of distros.

Should compile and work on big-endian machines too. All the htole16, htole32 etc. macros are in place, but never tested.

### Compile-time Dependencies

Subspaced requires four statically linked libraries ([OpenSSL](https://github.com/openssl/openssl),
[cURL](https://github.com/curl/curl), [libjpeg](http://www.ijg.org) and [SQLite3](https://www.sqlite.org/)). To
download and compile all of those dependencies, simply issue

```sh
$ make libs
```

Which should create the following files:

```
5.6M	lib/libcrypto.a
728K	lib/libcurl.a
1.7M	lib/libjpeg.a
1.1M	lib/libssl.a
1.1M	lib/sqlite3.o
```

Once you have these files and the include files in openssl/include, curl/include, libjpeg and sqlite3, there's no need to re-run
this command any more. After you have compiled subspaced, you can check the libraries with `subspaced --version`.

NOTE: SQLite3 is special, as it's distributed as a single C file which needs an additional encryption codec implementation. As
such lib/sqlite3.o will be created when you compile subspaced for the first time.

IMPORTANT: don't open tickets about compiling these libraries. We don't have the will nor the capacity to support them, so
please turn to their respective support / helpdesk / issue tracker / irc channel / whatever, or search forums for solutions.

### Compiling subspaced

To compile the SubSpace Market daemon itself, type

```sh
$ make
```

This shouldn't take long, few minutes at worst. That's all folks!

NEXT: [Configure SubSpace Market](https://gitlab.com/subspaced/subspaced/blob/master/docs/configure.md)

