Running subspaced
=================

PREV: [Configuring SubSpace Market daemon](https://gitlab.com/subspaced/subspaced/blob/master/docs/configure.md)

Now that you have a configuration file, you can run the SubSpace Market daemon without entering configuration mode.

```sh
$ subspaced
```

I suggest to run it automatically on boot up as a system service. In this mode the main process is detached from the controlling
terminal, and messages are not shown on standard error stream, but redirected to '~/.subspaced/log'.

```sh
$ subspaced --daemon
```

I18N
----

The language of the market's interface is autodetected from the environment variable LANG, but you can override that by specifying
the language code as an argument (except when it's forced with 'lang=' in the configuration file):

```sh
$ LANG="es" subspaced
$ subspaced -l ru
```

You can list available language codes with '--help'. This only guarantees that the
[interface will be translated](https://gitlab.com/subspaced/subspaced/blob/master/docs/translate.md), but you'll see the product's
name and description in English if the vendor couldn't translate their product's profile in your language.

Log messages are always in English, they are never translated so that you can parse the log with a script if you want.

Entering the Market
-------------------

Open up your browser (or TBB), and navigate to [https://localhost:8080/](https://localhost:8080/). It's very likely that your
browser will complain about invalid certificate (ERR_CERT_AUTORHITY_INVALID or ERROR_SELF_SIGNED_CERT), but don't be scared,
that's normal. It's perfectly safe to click 'Advanced' and then 'Add Exception' or to click 'Proceed to localhost'. The reason
for this warning is, there's no way to get a valid, third-party verified certificate for the CN 'localhost'.

If you care about OPSEC, you should disable JavaScript. The SubSpace Market interface does not need it at all, it only uses JS
to automatically focus input fields.

NEXT: [Buyer how-to](https://gitlab.com/subspaced/subspaced/blob/master/docs/buyer.md)
