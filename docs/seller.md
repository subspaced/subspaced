Seller HOW-TO
=============

PREV: [Buyer how-to](https://gitlab.com/subspaced/subspaced/blob/master/docs/buyer.md)

How to Became a Seller
----------------------

There's no vendor verification. This is a free market, any user who can buy can also sell. You only have to
[set up a product profile](https://gitlab.com/subspaced/subspaced/blob/master/docs/product.md) for that. Once done,
the profile will be distributed to the peer nodes, so anybody can see it even if your subspaced is not running. You
will see the orders next time you go on-line on the market.

As an extreme measure, you can install subspaced on a Raspberry Pi, which you physically hide somewhere and which
is on-line 24/7, and you can connect to the market [remotely](https://gitlab.com/subspaced/subspaced/blob/master/docs/remote.md)
from your home or from a public hotspot.

Keep in mind having several layers of security (like [VPN](https://gitlab.com/subspaced/subspaced/blob/master/docs/vpn.md)
for example), an up-to-date OS with all the security patches and paranoid OPSEC is essential when you want to receive funds
truly anonymously.

Once a buyer clicks on an "Confirm Order" link of one of your products, you'll receive a new message and you'll see the
details under the "Orders" page.

1. Order Received
-----------------

When somebody wants to buy your product, you will receive a request. You either have to decline it (if you can't
fullfil, or because you think the buyer has too many down votes), or accept it and label the product with the
buyer's name (so that you won't sell the same product to somebody else).

If you've set up your product profile to require a shipping address, you'll receive that too. That way you'll see
if the buyer wants the product to be shipped to an address where the deliveries are known to be lost, and you can
decline the order.

If the buyer wants an escrow, you'll see that here too. If you agree to the escrow, the order is handled outside
of the market, go to step 5.

If you're doing an auction, then you don't have to do anything. You'll receive many orders for the same product, but at the
end of the day, only accept one of them (with the highest bid probably, but not necessairly). The other orders will be declined
automatically.

2. Sending Your Public Key
---------------------------

Either you use your own wallet, or create a new one for this transaction (recommended). If you have set up
[bitcoind](https://gitlab.com/subspaced/subspaced/blob/master/docs/bitcoind.md), then this step is done automatically.
Otherwise you have to enter these commands after your wallet application's name (for example `bitcoin-cli`) in the command line:

```sh
getnewaddress
AAA
validateaddress AAA
...
 "pubkey": "AAAPUB"
...
```

You have to copy'n'paste your public key (AAAPUB) into a form on the market to acknowledge the order. Copy the address
(AAA) instead if the buyer has selected direct payment (only available for localonly networks). Your input will be
pattern matched, so that you won't mess up.

3. Waiting for Deposit
----------------------

The buyer pays a deposit, and sends the transaction id (DDD) back to you. You can use the trid to verify if the full price for
the goods (0.1 BTC in this example) is in the wallet (wait for several confirmations). This step is automatic if you have set
up bitcoind.

```sh
getrawtransaction DDD 1
```

Look for the fields "amount" and "confirmations".

4. Payment Request and Shipment
-------------------------------

Once it's confirmed that the money is in the multisig wallet, you have to enter a payout wallet (XXX) and select miner's fee.
A raw payout transaction will be created (EEE), which you have to sign with AAA wallet's private key. This part is done
automatically if you have set up bitcoind.

```sh
signrawtransaction EEE
...
 "hex": "FFF"
...
```

Now that you have done your part about the payout, you have to send the goods to the buyer IRL. You should receive either a
shipment id or a *Delivery Tracking URL*, which you have to tell the buyer so that they can track the package en route. If
you're selling digital goods, this is the place to enter the *Unique Download URL* (can be any file sharing service or pastebin URL,
but see section "Selling Digital Goods" in [product profile](https://gitlab.com/subspaced/subspaced/blob/master/docs/product.md)
documentation to learn how SubSpace Market can help you with that).

Last thing, you have to copy the signed raw transaction (FFF) and the tracking url into the form on the market.

5. Finalizing
-------------

Once the buyer has received the goods, they will sign your transaction (FFF) and release the funds from the deposit
to your wallet (XXX). It is possible that the buyer doesn't have bitcoind connection up and running, in that case you'll
get a signed version of the FFF transaction (GGG), and you'll be redirected to a "Broadcast transaction" page.

The buyer will also rate the quality of your product and overall service of yours. Always try to be correct, because buyers
won't order from you if you have bad reputation. But this is a two way street, you also have to rate the buyer.

NEXT: [Disputes and refund](https://gitlab.com/subspaced/subspaced/blob/master/docs/disputes.md)
