Configure a VPN (OPTIONAL)
==========================

Although it's optional, we strongly recommend to use a VPN tunnel to hide your traffic from your ISP. We recommend openvpn, but
any other VPN software should be fine.

VPN Tunnel and Routing
----------------------

Always check if your tunnel really works. On Linux, you can query the routing table with

```sh
ip route show | grep 0.0.0.0
```

If you see a line containing "0.0.0.0/1 via x.x.x.x dev tun0" then you are fine, all your network traffic is routed into the
VPN tunnel.

NEXT: [Configure anonymizer](https://gitlab.com/subspaced/subspaced/blob/master/docs/anonymizer.md)

