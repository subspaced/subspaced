SubSpace Market Markdown
========================

In messages, product descriptions, and motd files, no HTML allowed, only UTF-8 plain text. Newlines will be handled correctly,
and urls will be converted to links. Minimal markdown also supported:

```
# heading 1
## heading 2
### heading 3
*emphasis*
`code`
[link](url)
```
