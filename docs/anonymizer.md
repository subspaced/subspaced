Configure Anonymizer
====================

PREV: [Configure VPN](https://gitlab.com/subspaced/subspaced/blob/master/docs/vpn.md)

NOTE: you only have to do this step once. You can set up both anonymizer networks, but it's enough to have one of them.

Using TorBrowser
----------------

Simplest solution is to use Tor Browser Boundle which already includes tor. Your torrc is stored under
'$BROWSERDIR/Browser/TorBrowser/Data/Tor/torrc', and you'll have to edit that. The only downside is, you have to run TBB in the
whole time. With a proxy daemon, you can close the browser anytime, and subspaced will still be able to synchronize with the
network.

Using Tor
---------

Normally [tor](https://torproject.org) proxy runs as a daemon with configuration in '/etc/tor/torrc', but the place may differ on
your distribution. Could be '/usr/local/etc/tor/torrc' as well. Once you've located the torrc file, add these lines at the end:

```
HiddenServiceDir $DataDirectory/hidden_service
HiddenServicePort 8080 127.0.0.1:8080
HiddenServiceVersion 3
```

Replace $DataDirectory with your directory of course (could be '$BROWSERDIR/Browser/TorBrowser/Data/Tor', '$HOME/.tor' or
'/var/lib/tor', etc.), you may find it in the torrc file, or you can pick any directory you like. Create the 'hidden_service'
directory there, and make sure it's only accessible by the owner (this step is important):

```sh
$ cd $DataDirectory
$ mkdir hidden_service
$ chmod 0700 hidden_service
```

You may need to set the owner too, if you are not running tor as your user:

```sh
$ sudo chown debian-tor hidden_service
```

Restart the tor system service (or TBB). Now open '$DataDirectory/hidden_service/hostname', your `Tor node name` is there ending
in '.onion'. If TBB complains about not being able to run tor, then check torrc syntax and the service directory's permissions.

With tor, once your Hidden Service is up, the SubSpace Market's peer to peer network can use that instantly.

Using I2P
---------

Another anonymizer is [I2P](https://geti2p.net) but unfortunately it's written in Java. There's a very cool C++ implementation
of the same protocol, called [i2pd](https://i2pd.website), use that instead (standard Ubuntu package). For both, you'll need to
add a Server Tunnel, because like tor, i2p does not allow incoming connections by default. With the
[Java version](https://geti2p.net/en/docs/how/tunnel-routing):

1. Create a "Standard server" tunnel in I2PTunnel.
2. Set the "Name" to "subspaced-in".
3. Set "Target Port" to "8080".
4. Check the "Auto Start" box.
5. Save the tunnel.

With [i2pd](https://i2pd.readthedocs.io/en/latest/user-guide/tunnels), you have to add these lines to '~/.i2pd/tunnels.conf':

```
[subspaced-in]
type = server
host = 127.0.0.1
port = 8080
keys = subspaced-in.dat
```

Then restart i2pd. It's not trivial to get the `I2P node name` for your tunnel. With Java, navigate to the I2PTunnel page, click on
"subspaced-in", and note the "Local Destination" string. With i2pd, check the logs for the name ending in '.b32.i2p'.

NOTE: it could take several hours, even days, before your tunnel gets announced on the I2P network and it can be used by the
SubSpace Market. Be patient, and check if your tunnel actually works before you run subspaced.

NEXT: [Configure bitcoind](https://gitlab.com/subspaced/subspaced/blob/master/docs/bitcoind.md)

