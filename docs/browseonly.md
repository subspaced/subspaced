SubSpace Market Browse Only Mode
================================

In this mode the market does not use SSL for it's web interface. The purpose of this mode is to run it on a (virtual) server to
advertise the products of the market, and to provide a (hopefully somewhat constant) bootstrap node for newcomers. Visitors will
be able to browse and search the market anonymously, without the need to install subspaced. In this mode user login, messaging,
ordering and all the other user related features won't work. Also there'll be some anti-scam and anti-phishing protection, like
a captcha challenge, and the browser's language will be autodetected from the browser (which can be overridden from a drop-down
menu).

Configuration
-------------

To enable this mode, set up a [tor](https://gitlab.com/subspaced/subspaced/blob/master/docs/anonymizer.md) Hidden Service
(or I2P Server Tunnel), [download](https://gitlab.com/subspaced/subspaced/tree/master/bin) subspaced on
your virtual server, and run (note the `-b` flag):

```sh
$ subspaced --configure -b
```

Configuration will skip user key generation and registration and will add `browseonly=1` to your '~/.subspaced/market.conf'.

Running
-------

Once configured and bootstraped to the SubSpace Market network, you just run subspaced as usual, probably in the background:

```sh
$ subspaced --daemon
```

After that your server will show the market's webpage. You can advertise your clearnet or Hidden Service URL, link that
on forums, etc.

Message of the Day
------------------

You can display an info message on the top of the front page by creating '~/.subspaced/motd'. For security reasons, no HTML
allowed in it, only UTF-8 plain text. Newlines will be handled correctly, and urls will be converted to links. Minimal
[markdown](https://gitlab.com/subspaced/subspaced/blob/master/docs/markdown.md) also supported in the motd file.

To translate motd, append dot langcode to the filename, like '~/.subspaced/motd.ru'.

Security Considerations
-----------------------

If you open your subspaced for the public, NEVER run it as root. Ports under 1024 (like the standard http port 80)
require root privileges to bind to, but you can drop those privs by adding a `user=` line to the
[configuration file](https://gitlab.com/subspaced/subspaced/blob/master/docs/configure.md).

Should not needed, but as an extreme measure you can run browse only subspaced in a chroot, use AppArmour (subspaced should
never open files outside of it's '~/.subspaced' data directory). But the best practice is, dedicate a small vm only for subspaced,
which has no other data on it, and in the unlikely event of a deface, can be restored from a snapshot within a blink of an eye.

Note that browse only nodes are listening to web interface connections on all interfaces (as opposed to normal mode, which listens
only on the loopback interface). If you want to make subspaced only accessible through tor or I2P, then block incoming connections
to subspaced port on all interfaces except "lo" in your firewall (which you should have anyway). Although in this mode subspaced
can accept user interface connections from any ip, p2p connections will still be restricted to loopback interface through tor or
I2P.

For security reasons, browse only nodes won't syncronize everything as a normal node would. Their knowledge of the SubSpace
Market network is strictly limited to peers list and products. Furthermore, only products that has the "show on browse only nodes"
flag set will be synced and listed.

NEXT: [Translator's guide](https://gitlab.com/subspaced/subspaced/blob/master/docs/translate.md)

