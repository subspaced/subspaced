Configure bitcoind (OPTIONAL)
=============================

PREV: [Configure anonymizer](https://gitlab.com/subspaced/subspaced/blob/master/docs/anonymizer.md) network

NOTE: you only have to do this step once.

Bitcoind
--------

Download the official [Bitcoin Core](https://github.com/bitcoin/bitcoin/releases) client. Other applications may work too provided
they implement the same [JSON RPC API](https://en.bitcoin.it/wiki/Original_Bitcoin_client/API_Calls_list), but not tested). If
you'd prefer the lightweight solution, we suggest to use something like
[pyrpcwallet](https://github.com/CounterpartyXCP/pyrpcwallet) or [sPRUNED](https://github.com/gdassori/spruned). In order to
enable the JSON RPC API, you have to add these lines to '~/.bitcoin/bitcoin.conf':

```
server=1
rpcbind=127.0.0.1
rpcallowip=127.0.0.1
rpcport=8332
rpcpassword=SomeReallyLongGeneratedPasswordEvenYouCantRemember
```

If you don't enable the API, you can still order in SubSpace Market, but you have to copy'n'paste the multisig commands into your
wallet application manually. The market's order process will guide you through if subspaced can't connect to Bitcoin Core. If you
want a GUI, run `bitcoin-qt` (with that, you'll have to enter the commands in "Help" / "Debug window" / "Console"). Otherwise to
run the wallet application in the background, use `bitcoind`, and enter the commands in the command line after `bitcoin-cli`.

NOTE: you should always restrict the RPC API to local host only. Don't allow remote access otherwise you'll get robbed pretty
easily. We strongly suggest to run bitcoind through tor, so also add this line to '~/.bitcoin/bitcoin.conf' for better security:

```
proxy=127.0.0.1:9050
```

NEXT: [Download or compile SubSpace Market daemon](https://gitlab.com/subspaced/subspaced/blob/master/docs/compile.md)

